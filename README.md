1. Install Docker.

2. Install K8.

3. Configure K8.

4. Clone the git repo locally.

5. Untar the images.
    docker load -i <tar_image_path>
6. Create Ingress
      kubectk apply -f /root/devopsproject/Deployment/nginx-ingress/*
7. Apply the deployments
      kubectl apply -f /root/devopsproject/Deployment/airports-assembly/*
      kubectl apply -f /root/devopsproject/Deployment/countries-assembly/*
8. check pods:
      watch kubectl get po -o wide
9. check services
      kubectl get svc -n <namespace>
10. curl the airports-service and country-service
      curl -L http://10.105.168.70:8080/countries/id?code=TT    #Replace the IP with the IP of the service
      curl -L http://10.105.168.70:8080/countries/id?code=TT    #Replace the IP with the IP of the service

      curl -L http://10.104.253.248:8080/airports/
      curl -L http://10.105.168.70:8080/countries
11. check health of the pods
      curl -L http://10.104.253.248:8080/health/live
         {"status":200,"state":"OK"}
      curl -L http://10.104.253.248:8080/health/ready
         { "status":200,"state":"OK"}

12. Upgrade the image of airports-assembly
Blue green deployment in place.
Change the image to new in the deployment file.
kubectl apply -f /root/devopsproject/Deployment/countries-assembly/*


